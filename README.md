# Test

La aplicación esta dividida en dos componentes

  - Front Angular 10
  - Backend microservicio
 
# Microservicio

Es necesario tener configurado e instalada la JDK versión 8 y Maven 3 o superior, configurados en el classpath del sistema operativo para poder ejecutar desde linea de comandos el proyecto.

  - JDK 8 https://www.oracle.com/mx/java/technologies/javase/javase-jdk8-downloads.html
  - Maven 3.5 https://maven.apache.org/download.cgi

Pasos para ejecutar el proyecto:
  - Descargar desde el repositorio git clone https://gitlab.com/polymathventures/api-polymath-test
    ```sh
    $ git clone https://gitlab.com/polymathventures/api-polymath-test
    $ cd api-polymath-test
    ```
  - Descargar las dependencias y construir el jar del proyecto, ademas de ejecutar las pruebas unitarias con maven.
     ```sh
    $ mvn clean package
    ```
  - Ejecutar el jar generado para hacer el run de la aplicación
     ```sh
    $ cd target
    $ java -jar api-polymath-test-0.0.1-SNAPSHOT.jar
    ```
    
  - La aplicación ya debe estar escuchando en el puerto 8080 que es la que viene configurada en el proyecto angular
     ```sh
    $ http://localhost:8080
    ```
# Front Angular

Es necesario tener instaladas las siguientes herramientas para su compilación y manejo de dependencias:

  - Npm (Viene por defecto cuando se instala nodejs) https://nodejs.org/es/download/package-manager/
  - Angular CLI npm install -g @angular/cli

Pasos para ejecutar el proyecto:
  - Descargar desde el repositorio git clone https://gitlab.com/polymathventures/front-polymath-test
    ```sh
    $ git clone https://gitlab.com/polymathventures/front-polymath-test
    $ cd front-polymath-test
    ```
  - Desde la carpeta del proyecto hacer un npm install para descargar las dependencias
     ```sh
    $ npm install
    ```
  - Ejecutar el comando de angular cli ng serve que se encargará de ejecutar el proyecto en http://localhost:4200 por default
     ```sh
    $ ng serve
    ```
