package com.polymath.api.test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.polymath.api.test.entity.Task;
import com.polymath.api.test.entity.User;
import com.polymath.api.test.service.ITaskService;
import com.polymath.api.test.service.IUserService;

/**
 * 
 * @author Enrique
 *
 */
@SpringBootTest
class ApiPolymathTestApplicationTests {
	
	@Autowired
	private ITaskService taskService;
	
	@Autowired
	private IUserService userService;
	
	/**
	 * 
	 */
	@Test
	@DisplayName("Get tasks ")
	public void getTasks() {
		List<Task> list=taskService.getTasks();		
		assertTrue(list != null);		
	}
	
	/**
	 * 
	 */
	@Test
	@DisplayName("Get tasks by userId pageable")
	public void getTasksByUserId() {
		List<Task> list=taskService.getTasksByUser(null,1, 0, 10);
		assertTrue(list != null);		
	}
	
	/**
	 * 
	 */
	@Test
	@DisplayName("Count tasks by userId")
	public void countTasksByUserId() {
		Integer count=taskService.countTasksByUser(null,1);
		assertTrue(count>0);		
	}
	
	/**
	 * 
	 */
	@Test
	@DisplayName("Get users ")
	public void getUsers() {
		List<User> list=userService.getUsers();		
		assertTrue(list != null);		
	}

	@Test
	@DisplayName("Get username ")
	public void getUser() {
		Optional<User> user=userService.getUser(1);
		
		assertTrue(user.isPresent());		
	}
	
}
