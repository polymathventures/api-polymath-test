package com.polymath.api.test.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.polymath.api.test.entity.Task;
import com.polymath.api.test.service.ITaskService;

/**
 * 
 * @author Enrique
 *
 */
@RestController
@RequestMapping(value="task")
public class TaskController {

	@Autowired
	private ITaskService taskService;
	
	/**
	 * 
	 * @return
	 */
	@GetMapping
	public ResponseEntity<List<Task>> getTasks() {
		return ResponseEntity.ok(taskService.getTasks());
	}
	
	/**
	 * 
	 * @param userId
	 * @return
	 */
	@PostMapping(value = "/{userId}/userId/{page}/page/{elements}/elements")
	public ResponseEntity<List<Task>> getTasksByUser(@PathVariable(value = "userId") Integer userId,
													 @PathVariable(value = "page") Integer page,
													 @PathVariable(value = "elements") Integer elements,
													 @RequestBody Task task) {
		return ResponseEntity.ok(taskService.getTasksByUser(task,userId,page, elements));
	}
	
	/**
	 * 
	 * @param userId
	 * @return
	 */
	@PostMapping(value = "/count/{userId}/userId")
	public ResponseEntity<Integer> countTasksByUser(@PathVariable(value = "userId") Integer userId,@RequestBody Task task) {
		return ResponseEntity.ok(taskService.countTasksByUser(task,userId));
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping(value = "/{id}")
	public ResponseEntity<Task> getTask(@PathVariable(value = "id") Integer id) {
		return ResponseEntity.ok(taskService.getTask(id).get());
	}
	
	/**
	 * 
	 * @param task
	 * @return
	 */
	@PostMapping
	public ResponseEntity<Task> saveTask(@RequestBody Task task) {		
		return ResponseEntity.status(201).body(taskService.saveTask(task));		
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@PutMapping(value="/{id}/status")
	public ResponseEntity<?> updateStatus(@PathVariable(value = "id") Integer id) {		
		Optional<Task> optional=taskService.getTask(id);
		if(optional.isPresent()) {
			Task task=optional.get();
			task.setStatus(1);
			;
			return ResponseEntity.status(200).body(taskService.saveTask(task));		
		}
		
		return ResponseEntity.status(400).build();
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> delete(@PathVariable(value = "id") Integer id) {
		taskService.deleteTask(id);
		return ResponseEntity.status(200).build();
	}
	
}
