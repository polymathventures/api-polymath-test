package com.polymath.api.test.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.polymath.api.test.entity.User;
import com.polymath.api.test.service.IUserService;

/**
 * 
 * @author Enrique
 *
 */
@RestController
@RequestMapping(value="user")
public class UserController {

	@Autowired
	private IUserService userService;
	
	/**
	 * 
	 * @return
	 */
	@GetMapping
	public ResponseEntity<List<User>> getUsers() {
		return ResponseEntity.ok(userService.getUsers());
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping(value = "/{id}")
	public ResponseEntity<User> getTask(@PathVariable(value = "id") Integer id) {
		return ResponseEntity.ok(userService.getUser(id).get());
	}
	
	/**
	 * 
	 * @param task
	 * @return
	 */
	@PostMapping(value = "/create")
	public ResponseEntity<User> saveUsuario(@RequestBody User user) {		
		return ResponseEntity.status(201).body(userService.saveUser(user));		
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> delete(@PathVariable(value = "id") Integer id) {
		userService.deleteUser(id);
		return ResponseEntity.status(200).build();
	}
}
