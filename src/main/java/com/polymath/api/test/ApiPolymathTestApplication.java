package com.polymath.api.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author Enrique
 *
 */
@SpringBootApplication
public class ApiPolymathTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiPolymathTestApplication.class, args);
	}

}
