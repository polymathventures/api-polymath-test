package com.polymath.api.test.service;

import java.util.List;
import java.util.Optional;
import com.polymath.api.test.entity.Task;

/**
 * 
 * @author Enrique
 *
 */
public interface ITaskService {

	List<Task> getTasks();
	List<Task> getTasksByUser(Task task,Integer userId,int page, int elements);
	Integer countTasksByUser(Task task,Integer userId);
	Optional<Task> getTask(Integer id);
	void deleteTask(Integer id);
	Task saveTask(Task task);
}
