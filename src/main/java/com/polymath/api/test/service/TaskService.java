package com.polymath.api.test.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.polymath.api.test.entity.Task;
import com.polymath.api.test.repository.ITaskRepository;
import com.polymath.api.test.repository.IUserRepository;

/**
 * 
 * @author Enrique
 *
 */
@Service
public class TaskService implements ITaskService {

	@Autowired
	private ITaskRepository taskRepository;
	
	@Autowired
	private IUserRepository userRepository;

	/**
	 * 
	 */
	@Override
	public List<Task> getTasks() {		
		return taskRepository.findAll();
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public Optional<Task> getTask(Integer id) {		
		return taskRepository.findById(id);
	}
	
	/**
	 * 
	 * @param id
	 */
	@Override
	public void deleteTask(Integer id) {		
		Task task= taskRepository.findById(id).get();
		taskRepository.delete(task);
	}
	
	/**
	 * 
	 * @param task
	 * @return
	 */
	@Override
	@Transactional
	public Task saveTask(Task task) {
		task.setCreationDate(new Date());
		task=taskRepository.save(task);
		task.setUser(userRepository.findById(task.getUser().getUserId()).get());
		return task;
	}

	/**
	 * 
	 */
	@Override
	public List<Task> getTasksByUser(Task task,Integer userId, int page, int elements) {
		Pageable pageable = PageRequest.of(page, elements);
		return taskRepository.findTaskByUser(userId,(task!=null && task.getDescription()!=null?task.getDescription():null),pageable);
	}

	/**
	 * 
	 * @param userId
	 * @return
	 */
	@Override
	public Integer countTasksByUser(Task task,Integer userId) {
		return taskRepository.countTaskByUser(userId,(task!=null && task.getDescription()!=null?task.getDescription():null));
	}
}
