package com.polymath.api.test.service;

import java.util.List;
import java.util.Optional;
import com.polymath.api.test.entity.User;

/**
 * 
 * @author Enrique
 *
 */
public interface IUserService {

	List<User> getUsers();
	Optional<User> getUser(Integer id);
	void deleteUser(Integer id);
	User saveUser(User user);
}
