package com.polymath.api.test.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.polymath.api.test.entity.User;
import com.polymath.api.test.repository.IUserRepository;
import static java.util.Collections.emptyList;

/**
 * 
 * @author Enrique
 *
 */
@Service
public class UserService implements IUserService, UserDetailsService  {

	@Autowired
	private IUserRepository userRepository;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	/**
	 * 
	 */
	public List<User> getUsers() {
		return userRepository.findAll();
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public Optional<User> getUser(Integer id) {		
		return userRepository.findById(id);
	}
	
	/**
	 * 
	 * @param id
	 */
	@Override
	public void deleteUser(Integer id) {		
		User user= userRepository.findById(id).get();
		userRepository.delete(user);
	}
	
	/**
	 * 
	 * @param task
	 * @return
	 */
	@Override
	@Transactional
	public User saveUser(User user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setCreationDate(new Date());
		return userRepository.save(user);
	}

	/**
	 * 
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user=userRepository.findByUsername(username);
		
		if (user == null) {
			throw new UsernameNotFoundException(username);
		}
		return new org.springframework.security.core.userdetails.User(user.getUserId()+"#"+user.getUsername(), user.getPassword(), emptyList());
		
	}

}
