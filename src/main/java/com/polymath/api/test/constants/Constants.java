package com.polymath.api.test.constants;

public class Constants {

		// Spring Security
		public static String LOGIN_URL = "/login";
		public static String HEADER_AUTHORIZACION_KEY = "Authorization";
		public static String TOKEN_BEARER_PREFIX = "Bearer ";

		// JWT
		public static String SECRET_KEY = "1234";
		public static long TOKEN_EXPIRATION_TIME = 3_600_000; // 1hour
}
