package com.polymath.api.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.polymath.api.test.entity.User;

/**
 * 
 * @author Enrique
 *
 */
public interface IUserRepository extends JpaRepository<User, Integer>{

	User findByUsername(String username);
}
