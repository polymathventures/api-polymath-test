package com.polymath.api.test.repository;

import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.polymath.api.test.entity.Task;

/**
 * 
 * @author Enrique
 *
 */
public interface ITaskRepository extends JpaRepository<Task, Integer>{

	/**
	 * 
	 * @param userId
	 * @param description
	 * @param pageable
	 * @return
	 */
	@Query("SELECT task FROM Task task WHERE task.user.userId= :userId "+
			"AND  LOWER(task.description) LIKE CASE WHEN :description IS NOT NULL THEN LOWER(CONCAT('%',:description,'%')) ELSE LOWER(task.description) END")
	List<Task> findTaskByUser(Integer userId,String description,Pageable pageable);
	
	/**
	 * 
	 * @param userId
	 * @param description
	 * @return
	 */
	@Query("SELECT COUNT(task) FROM Task task WHERE task.user.userId= :userId "+
			"AND  LOWER(task.description) LIKE CASE WHEN :description IS NOT NULL THEN LOWER(CONCAT('%',:description,'%')) ELSE LOWER(task.description) END")
	Integer countTaskByUser(Integer userId,String description);
	
}
